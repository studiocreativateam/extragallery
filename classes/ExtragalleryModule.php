<?php

class ExtragalleryModule
{
    /**
     * @var $module extragallery
     */
    private $module;

    public function __construct($module)
    {
        $this->module = $module;
        $this->context = Context::getContext();
    }

    /**
     * @param  string  $prefix  (adding in url parameter name)
     * @return string url
     * @throws PrestaShopException
     */
    public function getModuleUrl($prefix = '')
    {
        $module_token = $this->context->link->getAdminLink('AdminModules').'&configure='.$this->module->name.'&tab_module='.$this->module->tab.'&module_name='.$this->module->name.$prefix;
        if ($this->module->psv >= 1.7) {
            return $module_token;
        }
        if ($this->module->psv == 1.6) {
            $host_webpath = Tools::getAdminUrl().$this->context->controller->admin_webpath;
        } else {
            $host_webpath = Tools::getHttpHost(true).__PS_BASE_URI__
                .Tools::substr(str_ireplace(_PS_ROOT_DIR_, '', getcwd()), 1);
        }
        return $host_webpath.'/'.$module_token;
    }
}