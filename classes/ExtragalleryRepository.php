<?php

class ExtragalleryRepository
{
    public static function create($file_name)
    {
        $context = Context::getContext();
        $success = Db::getInstance()->insert('extragallery_files', ['id_shop' => $context->shop->id, 'file_name' => $file_name]);
        if ($success) {
            return (int)Db::getInstance()->getValue('SELECT `id` FROM `' . _DB_PREFIX_ . 'extragallery_files` ORDER BY `id` DESC');
        }
        return null;
    }

    public static function delete($id)
    {
        $result = Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'extragallery_product_file` WHERE id_extragallery_file = ' .$id);
        $result &= Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'extragallery_files` WHERE id = ' .$id);
        return $result;
    }

    public static function attachToProduct($id, $id_product)
    {
        return Db::getInstance()->insert('extragallery_product_file', ['id_product' => $id_product, 'id_extragallery_file' => $id]);
    }

    public static function getForProduct($id_product)
    {
        $context = Context::getContext();
        return Db::getInstance()->executeS('SELECT ef.file_name, ef.id FROM `' . _DB_PREFIX_ . 'extragallery_product_file` epf RIGHT JOIN `' . _DB_PREFIX_ . 'extragallery_files` ef ON ef.id=epf.id_extragallery_file WHERE ef.id_shop = ' . $context->shop->id . ' AND epf.id_product=' . $id_product);
    }
}
