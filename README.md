# Prestashop module Extragallery 😎

![drawing](logo.png?raw=true)

## About
A module to add an extragallery to products.
### Usage
`{hook h="displayExtragallery"}`

![img.png](img/img.png)

## Contributing+
Studio Creativa Sp. z o.o. Copyright (c) permanent

![drawing](img/sc-logo.png?raw=true)

## Requirements
Require PrestaShop e-commerce solution.