<div id="product-extragallery-images">
    <div class="slider-gallery">
    {foreach $images as $image}
        {if $image.file_name != 'index.php'}
            <a data-fancybox="gallery2" href="{$path|escape}upload/{$image.file_name|escape:url}" class="gallery-item" style="background: url({$path|escape}upload/{$image.file_name|escape:url}) 50%/cover no-repeat"></a>
        {/if}
    {/foreach}
    </div>
    <div class="slider-navs"></div>
</div>
