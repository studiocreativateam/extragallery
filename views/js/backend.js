document.addEventListener("DOMContentLoaded", () => {
    const form = document.querySelector('form.product-page');
    if (!form) return;

    const extragallery = form.querySelector('#product-extragallery-images');
    const items = extragallery.querySelector('.product-extragallery-images-dynamic-items');
    const extragallery_upload_file = extragallery.querySelector('input[name="extragallery_upload_file"]');
    const wrapper = extragallery_upload_file.closest('.extragallery-wrapper');
    const input_ids = extragallery.querySelector('input[type="hidden"][name="extragallery_files"]');

    extragallery_upload_file.addEventListener('change', e => {
        uploadFile();
    });

    document.addEventListener('click', e => {
        if (e.target.id === 'extragallery_upload_file-selectbutton' || e.target.closest('#extragallery_upload_file-selectbutton')) {
            extragallery_upload_file.click();
        }
        if (e.target.classList.contains('product-extragallery-images-delete-btn') || e.target.closest('.product-extragallery-images-delete-btn')) {
            const item = e.target.closest('.product-extragallery-images-dynamic-item');
            remove(item)
        }
    })

    function add(url, id) {
        const item = document.createElement('div');
        item.classList.add('product-extragallery-images-dynamic-item');
        item.style.backgroundImage = `url('${url}')`;
        item.dataset.id = id;

        const btnDlt = document.createElement('div');
        btnDlt.classList.add('product-extragallery-images-delete-btn');

        const btnDltImg = document.createElement('img');
        btnDltImg.src = `${extragallery.dataset.modulePath}/img/close.png`;
        btnDlt.appendChild(btnDltImg);
        item.appendChild(btnDlt);
        items.appendChild(item);

        const arr = input_ids.value ? JSON.parse(input_ids.value) : [];
        const index = arr.indexOf(parseInt(id));
        if (index === -1) {
            arr.push(id);
            input_ids.value = JSON.stringify(arr);
        }
    }

    function remove(item) {
        const arr = input_ids.value ? JSON.parse(input_ids.value) : [];
        const res = [];
        Object.keys(arr).forEach(key => res[key] = parseInt(arr[key]));
        const index = res.indexOf(parseInt(item.dataset.id));
        delete res[index];
        input_ids.value = JSON.stringify(res.join('').split(''));
        item.remove();
    }

    function uploadFile() {
        const formData = new FormData();
        Object.keys(extragallery_upload_file.files).forEach(index => formData.append('files[]', extragallery_upload_file.files[index]));
        formData.append('ajax', true);
        formData.append('action', 'upload');
        formData.append('secure_key', wrapper.dataset.secureKey);
        formData.append("psv", wrapper.dataset.psv);
        fetch(wrapper.dataset.uploadUrl, {
            method: 'post',
            dataType: "JSON",
            body: formData,
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.data) {
                Object.keys(data.data).forEach(key => {
                    const row = data.data[key];
                    add(row.preview_url, row.id)
                })
            }
        })["catch"](function (error) {
            console.log(error);
        });
    }
});