<div id="product-extragallery-images" class="mb-4" data-module-path="{$path|escape}">
    <div id='topform-label'>
        <div class="row">
            <div class="col-12 h2 top-module-description">
                Extra gallery
            </div>
        </div>
    </div>
    <div class="product-extragallery-images-dynamic-items">
        {foreach $images as $image}
            {if $image.file_name != 'index.php'}
                <div class="product-extragallery-images-dynamic-item" style="background-image: url('{$path|escape}upload/{$image.file_name|escape:url}')" data-id="{$image.id}">
                    <div class="product-extragallery-images-delete-btn">
                        <img src="{$path|escape}img/close.png" alt="DELETE"/>
                    </div>
                </div>
            {/if}
        {/foreach}
    </div>

    <input type="hidden" name="extragallery_files" value="{array_column($images, 'id')|json_encode}">

    <div class="extragallery-wrapper" data-upload-url="{$extragallery_upload_url}" data-secure-key="{$extragallery_secure_key}" data-psv="{$extragallery_psv}">
        <input id="extragallery_upload_file" type="file" name="extragallery_upload_file" multiple class="hide" accept="image/*">
        <div class="input-group">
            <span class="input-group-btn">
                <button id="extragallery_upload_file-selectbutton" type="button" name="submitAddAttachments" class="btn btn-default">
                    <i class="icon-folder-open"></i> {l s='Add file' mod='fbpixelcatalog'}
                </button>
            </span>
        </div>
    </div>
</div>
