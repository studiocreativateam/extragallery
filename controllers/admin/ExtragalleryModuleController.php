<?php

class ExtragalleryModuleController extends ModuleAdminController
{
    public function __construct()
    {
        $this->secure_key = Tools::getValue('secure_key');
        parent::__construct();
    }

    public function init()
    {
        parent::init();
        if ($this->secure_key == $this->module->secure_key) {
            switch (Tools::getValue('action')) {
                case 'upload':
                    die(json_encode([
                        'data' => $this->module->upload($_FILES['files'] ?? []),
                    ]));
            }
        }
    }
}
