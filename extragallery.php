<?php

use PrestaShop\PrestaShop\Adapter\SymfonyContainer;

if (!defined('_PS_VERSION_')) {
    exit;
}

class extragallery extends Module
{
    public $psv;
    public $extragalleryModule;

    public function __construct()
    {
        $this->name = 'extragallery';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Studio Creativa';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = ['min' => '1.7', 'max' => _PS_VERSION_];
        $this->bootstrap = true;
        $this->secure_key = Tools::hash($this->name);
        $this->psv = (float)Tools::substr(_PS_VERSION_, 0, 3);

        parent::__construct();

        $this->displayName = $this->l('Extra Gallery');
        $this->description = $this->l('Adds an additional gallery to the product page.');
        $this->complete_content_files_location = dirname(__FILE__).'/upload/';

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall the Extra Gallery module?');

        require_once _PS_MODULE_DIR_.$this->name.'/classes/ExtragalleryModule.php';
        require_once _PS_MODULE_DIR_.$this->name.'/classes/ExtragalleryRepository.php';
        $this->extragalleryModule = new ExtragalleryModule($this);
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->createTables() ||
            !$this->registerHook('displayExtragallery') ||
            !$this->registerHook('displayAdminProductsMainStepLeftColumnBottom') ||
            !$this->registerHook('displayBackOfficeHeader') ||
            !$this->registerHook('actionProductSave') ||
            !$this->registerHook('actionProductAdd')) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !$this->dropTables()) {
            return false;
        }

        return true;
    }


    public function createTables()
    {
        $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'extragallery_files`(
			`id` int(10) unsigned NOT NULL auto_increment,
			`id_shop` int(10) unsigned NOT NULL,
			`file_name` varchar(200) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
        $res = Db::getInstance()->execute($sql);

        $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'extragallery_product_file`(
			`id` int(10) unsigned NOT NULL auto_increment,
			`id_extragallery_file` int(10) unsigned NOT NULL,
			`id_product` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id`),
            INDEX (`id_extragallery_file`),
            INDEX (`id_product`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
        $res &= Db::getInstance()->execute($sql);

        return $res;
    }

    public function dropTables()
    {
        $sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'extragallery_files`';
        $res = Db::getInstance()->execute($sql);

        $sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'extragallery_product_file`';
        $res &= Db::getInstance()->execute($sql);

        return $res;
    }

    public function hookDisplayExtragallery($params)
    {
        $this->context->smarty->assign([
            'images' => ExtragalleryRepository::getForProduct($params['product']['id_product']),
            'path' => $this->_path,
        ]);

        return $this->fetch('module:extragallery/views/front/gallery.tpl');
    }

    public function hookDisplayAdminProductsMainStepLeftColumnBottom($params)
    {
        $this->registerHook('displayExtragallery');
        $this->context->smarty->assign([
            'images' => ExtragalleryRepository::getForProduct($params['id_product']),
            'path' => $this->_path,
            'imagesExtensions' => ['jpg', 'gif', 'png', 'webp'],
        ]);

        return $this->fetch('module:extragallery/views/admin/form.tpl');
    }

    public function hookActionProductAdd($params)
    {

    }

    public function hookActionProductSave($params)
    {
        if (Tools::getValue('form')) {
            $forProduct = array_map('intval', array_column(ExtragalleryRepository::getForProduct($params['product']->id), 'id'));
            $files = array_map('intval', json_decode(Tools::getValue('extragallery_files'), true));
            foreach ($forProduct as $fileId) {
                if (!in_array($fileId, $files)) {
                    ExtragalleryRepository::delete($fileId);
                }
            }
            foreach ($files as $fileId) {
                if (!in_array($fileId, $forProduct)) {
                    ExtragalleryRepository::attachToProduct($fileId, $params['product']->id);
                }
            }
        }
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        // add styling
        $this->context->controller->addCSS($this->_path.'/views/css/backend.css', 'all');
        $this->context->controller->addJS($this->_path.'/views/js/backend.js', 'all');

        $this->smarty->assign([
            'extragallery_upload_url' => $this->context->link->getAdminLink('ExtragalleryModule'),
            'extragallery_secure_key' => $this->secure_key,
            'extragallery_psv' => $this->psv,
        ]);
    }

    public function upload(array $files)
    {
        $result = [];
        foreach ($files as $key => $values) {
            foreach ($values as $index => $value) {
                $result[$index] = $result[$index] ?? [];
                $result[$index][$key] = $value;
            }
        }
        $response = [];
        foreach ($result as $file) {
            $response[] = $this->processFileUpload($file);
        }
        return array_filter($response);
    }

    private function processFileUpload($file)
    {
        if (!is_writable($this->complete_content_files_location)) {
            return false;
        }

        if (file_exists($this->complete_content_files_location.$file['name'])) {
            $tmp_name = explode('.', $file['name']);
            $tmp_ext = end($tmp_name);
            array_pop($tmp_name);
            $tmp_name = implode('.', $tmp_name);
            $tmp_new_img_name = $this->complete_content_files_location.$tmp_name;

            $control_loop = false;
            $tmp_i = 1;
            while ($control_loop == false) {
                if (file_exists($tmp_new_img_name.'('.$tmp_i.').'.$tmp_ext)) {
                    ++$tmp_i;
                } else {
                    $file['name'] = $tmp_name.'('.$tmp_i.').'.$tmp_ext;
                    $control_loop = true;
                }
            }
        }
        $success = move_uploaded_file($file['tmp_name'], $this->complete_content_files_location.$file['name']);
        if ($success) {
            $id = ExtragalleryRepository::create($file['name']);
            if ($id) {
                return [
                    'id' => $id,
                    'preview_url' => $this->context->link->getBaseLink(). "modules/$this->name/upload/{$file['name']}",
                ];
            }
        }
        return null;
    }
}